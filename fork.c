/*
	Projekt 1 do predmetu POS
	Program v 3 procesoch
	Klara Mihalikova
	xmihal05
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>       
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED 1 /* XPG 4.2 - needed for WCOREDUMP() */

//globalna konstanta pre vypis termination signalu
const char *dumpConst = "";
const char *gFather = "grandparent";
const char *father = "parent";
const char *son = "child";

void printProcInfo(const char *label){
	//vypise informacie o danom procese
	printf("%s identification: \n", label); /*grandparent/parent/child */
	printf("	pid = %d,	ppid = %d,	pgrp = %d\n", (int)getpid(), (int)getppid(), (int)getpgrp());
	printf("	uid = %d,	gid = %d\n", (int)getuid(), (int)getgid());
	printf("	euid = %d,	egid = %d\n", (int)geteuid(), (int)getegid());
}

void printExitInfo(pid_t pid, int status, const char *label){
	//vypise dovod ukoncenia child procesu
	printf("%s exit (pid = %d):", label, (int)pid); /* and one line from */
	if(WIFEXITED(status))	//ak je wifexited true, child procesu sa ukoncil spravne
		// WEXITSTATUS vracia kod s ktorym sa child ukoncil v pripade WIFEXITED == true
		printf("	normal termination (exit code = %d)\n", WEXITSTATUS(status)); /* or */
	else if(WIFSIGNALED(status))	//ak true, vieme zistit ktory signal vyvolal child process
		//WTERMSIG() vrati kod signalu
		printf("	signal termination %s(signal = %d)\n", dumpConst, WTERMSIG(status)); /* or */
	else printf("	unknown type of termination\n");
}

/* ARGSUSED */
int main(int argc, char *argv[])
{
	//ziskam pid praotca
	pid_t pid, wpid;
	int ret_status;

	//WCOREDUMP() nemusi byt dostupne na vsetkych systemoch
	//prebrane z manualovych stranok 
	//		http://man7.org/tlpi/code/online/book/procexec/print_wait_status.c.html
	#ifdef COREDUMP
		strcpy(dumpConst, "with core dump ");
	#endif

	printProcInfo(gFather);

	pid = fork();
	if(pid == 0){
		//childProcess "otec"		
		printProcInfo(father);
		
		pid = fork();
		if(pid == 0){
			//childProcess "syn"
			printProcInfo(son);
			//nahradim proces programom danym prvym parametrom prikazoveho riadku
			//execv(subor, parametre)
			if(execv(argv[1], &argv[1]) == -1){
				perror("Error on execution of execv()");
				_exit(EXIT_FAILURE);
			}
		}
		else if(pid == -1){
			perror("Error on forking parent process");
			_exit(EXIT_FAILURE);
		}
		else{
			//v otcovi cakam na ukoncenie syna
			wpid = wait(&ret_status);
			printExitInfo(wpid, ret_status, son);
		}		
	}
	else if(pid == -1){
		//chyba
		perror("Error on forking grandparent process");
		exit(EXIT_FAILURE);
	}
	else{
		//som v praotcovi cakam na ukoncenie otca
		wpid = wait(&ret_status);
		printExitInfo(wpid, ret_status, father);
	}
	//praotec ukonci beh programu
	exit(EXIT_SUCCESS);
}