# Makefile ku projektu 1 do predmetu POS
# Klara Mihalikova, xmihal05

CC=gcc
CFLAGS=-Wall -g -pedantic -std=c11
RESULT = fork

$(RESULT): $(RESULT).c
	$(CC) $(CFLAGS) -o $(RESULT) $(RESULT).c

clean:
	rm -f $(RESULT) core*

